package de.bsc.proto.Database.DB;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashSet;

import de.bsc.proto.Database.Modell.Tours;

public class DBConfig extends OrmLiteConfigUtil {

    public static void main(String[] args) throws SQLException, IOException {
        writeConfigFile(new File("app\\src\\main\\res\\raw\\ormlite_config.txt"), getEntries());
    }

    static Class<?>[] getEntries() {
        LinkedHashSet<Class> dbEntries = new LinkedHashSet<>();
        dbEntries.add(Tours.class);
        return dbEntries.toArray(new Class[dbEntries.size()]);
    }
}