package de.bsc.proto.Database.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import de.bsc.proto.Database.Modell.BaseModell;
import de.bsc.proto.R;

/**
 * Created by Kenny Seyffarth on 15.12.2015.
 */
public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "tours.sqlite";

    private static String DATABASE_PATH;

    private Context myContext;

    private Class[] DB_ENTRIES = DBConfig.getEntries();

    public static DBHelper create(Context context) {
        DATABASE_PATH = "/data/data/" + context.getPackageName() + "/" + "databases/";
        return new DBHelper(context);
    }

    private DBHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion, int configFileId) {
        super(context, databaseName, factory, databaseVersion, configFileId);

    }

    private DBHelper(final Context context) {
        super(context, DATABASE_PATH + DATABASE_NAME, null, 1, R.raw.ormlite_config);
        this.myContext = context;


        try {
            File dir = new File(DATABASE_PATH);
            dir.mkdirs();
            File f = new File(DATABASE_PATH + DATABASE_NAME);
            if (!f.exists()) {
                f.createNewFile();
            }

            InputStream myinput = context.getAssets().open(DATABASE_NAME);
            OutputStream myoutput = new FileOutputStream(f, false);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myinput.read(buffer)) > 0) {
                myoutput.write(buffer, 0, length);
            }
            myoutput.flush();
            myoutput.close();
            myinput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            for (Class<? extends BaseModell> clazz : DB_ENTRIES) {
                int res = TableUtils.createTableIfNotExists(connectionSource, clazz);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

}