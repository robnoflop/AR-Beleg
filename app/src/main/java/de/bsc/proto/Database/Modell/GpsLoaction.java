package de.bsc.proto.Database.Modell;

/**
 * Created by RobNoFlop on 22/10/2017.
 */

public class GpsLoaction {
    public double latitude;
    public double longitude;

    public GpsLoaction(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
