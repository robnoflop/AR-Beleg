package de.bsc.proto.Database.Modell;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class Informations extends NotGeneratedModell {

    public static final String TEXT = "text";

    @DatabaseField
    private String text;

    public Informations() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
