package de.bsc.proto.Database.Modell;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class InformationsVisualisations extends NotGeneratedModell {

    public static final String CENTER_LOCATION = "centerLocation";

    @DatabaseField
    private String method;

    public InformationsVisualisations() {

    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
