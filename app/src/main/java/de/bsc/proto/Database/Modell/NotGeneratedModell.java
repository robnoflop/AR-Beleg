package de.bsc.proto.Database.Modell;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public abstract class NotGeneratedModell extends BaseModell {

    public static final String ID = "id";

    @DatabaseField(generatedId = false, unique = true)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
