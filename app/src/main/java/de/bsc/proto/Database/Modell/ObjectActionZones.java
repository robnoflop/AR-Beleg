package de.bsc.proto.Database.Modell;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.DatabaseField;

import java.lang.reflect.Type;
import java.util.List;


public class ObjectActionZones extends BaseModell{

    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String POINTS = "points";
    public static final String OBJECTS_REF = "objects_ref";

    @DatabaseField
    private int id;

    @DatabaseField
    private int type;

    @DatabaseField
    private String points;

    @DatabaseField
    private int  objects_ref;

    public ObjectActionZones() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public int getObjects_ref() {
        return objects_ref;
    }

    public void setObjects_ref(int objects_ref) {
        this.objects_ref = objects_ref;
    }

    public List<GpsLoaction> getPointsList() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Type listType = new TypeToken<List<GpsLoaction>>() {}.getType();
        List readFromJson = gson.fromJson(points, listType);
        return (List<GpsLoaction>) readFromJson;
    }

    public boolean isLocationInZone(GpsLoaction location) {
        boolean inside = false;
        List<GpsLoaction> polygon = getPointsList();
        for (int i = 0; i < polygon.size(); i++) {
            int j = i + 1;
            if (j >= polygon.size()) {
                j = 0;
            }


            if ((polygon.get(i).latitude > location.latitude) != (polygon.get(j).latitude > location.latitude) &&
                    (location.longitude < (polygon.get(j).longitude - polygon.get(i).longitude) * (location.latitude - polygon.get(i).latitude) / (polygon.get(j).latitude - polygon.get(i).latitude) + polygon.get(i).longitude)) {
                inside = !inside;
            }


            /*if(((polygon.get(i).latitude <= location.latitude) && (polygon.get(j).latitude > location.latitude)) ||
               ((polygon.get(i).latitude > location.latitude) && (polygon.get(j).latitude <= location.latitude))) {
                double vt = (location.latitude - polygon.get(i).latitude) / (polygon.get(i).latitude - polygon.get(i).latitude);
                if (location.longitude < polygon.get(i).longitude + vt * (polygon.get(j).longitude - polygon.get(i).longitude)) {
                    inside = !inside;
               }
            }*/
        }
        return inside;
    }
}
