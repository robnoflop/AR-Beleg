package de.bsc.proto.Database.Modell;

import android.location.Location;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class Objects extends NotGeneratedModell {

    public static final String NAME = "name";
    public static final String TAGS = "tags";
    public static final String TOURREF = "tourRef";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";

    @DatabaseField
    private String name;

    @DatabaseField
    private String tags;

    @DatabaseField
    private int tourRef;

    @DatabaseField
    private double longitude;

    @DatabaseField
    private double latitude;

    public Objects() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getTourRef() {
        return tourRef;
    }

    public void setTourRef(int tourRef) {
        this.tourRef = tourRef;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Location getLocation() {
        Location l = new Location("");
        l.setLatitude(latitude);
        l.setLongitude(longitude);
        return l;
    }
}
