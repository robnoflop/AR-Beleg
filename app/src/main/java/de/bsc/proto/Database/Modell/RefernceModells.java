package de.bsc.proto.Database.Modell;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class RefernceModells extends NotGeneratedModell {

    public static final String MODELL = "modell";
    public static final String DETECTIONMETHOD = "detectionMethod";
    public static final String OBJECTSREF = "objectsRef";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String DESCRIPTORS = "descriptors";
    public static final String DIERECTION = "direction";
    public static final String CONTOURMATCHINGDESCRIPTOR = "countourMatchingDescriptor";

    @DatabaseField
    private String modell;

    @DatabaseField
    private String detectionMethod;

    @DatabaseField
    private int objectsRef;

    @DatabaseField
    private double longitude;

    @DatabaseField
    private double latitude;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] descriptors;

    @DatabaseField
    private int direction;

    @DatabaseField
    private String countourMatchingDescriptor;

    public RefernceModells() {
    }

    public String getModell() {
        return modell;
    }

    public void setModell(String modell) {
        this.modell = modell;
    }

    public String getDetectionMethod() {
        return detectionMethod;
    }

    public void setDetectionMethod(String detectionMethod) {
        this.detectionMethod = detectionMethod;
    }

    public int getObjectsRef() {
        return objectsRef;
    }

    public void setObjectsRef(int objectsRef) {
        this.objectsRef = objectsRef;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public byte[] getDescriptors() {
        return descriptors;
    }

    public void setDescriptors(byte[] descriptors) {
        this.descriptors = descriptors;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getCountourMatchingDescriptor() {
        return countourMatchingDescriptor;
    }

    public void setCountourMatchingDescriptor(String countourMatchingDescriptor) {
        this.countourMatchingDescriptor = countourMatchingDescriptor;
    }
}
