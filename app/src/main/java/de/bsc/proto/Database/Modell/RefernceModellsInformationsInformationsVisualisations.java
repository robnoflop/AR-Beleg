package de.bsc.proto.Database.Modell;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class RefernceModellsInformationsInformationsVisualisations {

    public static final String REFERENCE_MODELL_REF = "RefernceModellsRef";
    public static final String INFORMATIONS_REF = "InformationsRef";
    public static final String INFORMATIONS_VISUALISATIONS_REF = "InformationsVisualisationsRef";

    @DatabaseField
    private int RefernceModellsRef;

    @DatabaseField
    private int InformationsRef;

    @DatabaseField
    private int InformationsVisualisationsRef;

    @DatabaseField
    private int positionX;

    @DatabaseField
    private int positionY;

    public RefernceModellsInformationsInformationsVisualisations() {
    }

    public int getRefernceModellsRef() {
        return RefernceModellsRef;
    }

    public void setRefernceModellsRef(int refernceModellsRef) {
        RefernceModellsRef = refernceModellsRef;
    }

    public int getInformationsRef() {
        return InformationsRef;
    }

    public void setInformationsRef(int informationsRef) {
        InformationsRef = informationsRef;
    }

    public int getInformationsVisualisationsRef() {
        return InformationsVisualisationsRef;
    }

    public void setInformationsVisualisationsRef(int informationsVisualisationsRef) {
        InformationsVisualisationsRef = informationsVisualisationsRef;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }
}
