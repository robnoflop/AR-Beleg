package de.bsc.proto.Database.Modell;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class Tours extends NotGeneratedModell {

    public static final String NAME = "name";

    @DatabaseField
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Tours() {

    }

    @Override
    public String toString() {
        return name;
    }
}
