package de.bsc.proto.Database.dao;

import android.content.Context;

import com.j256.ormlite.support.ConnectionSource;

import de.bsc.proto.Database.DB.DBHelper;
import de.bsc.proto.Database.Modell.ObjectActionZones;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class DaoManager {
    public static ToursDao toursDao;
    public static ObjectsDao objectsDao;
    public static RefernceModellsDao refernceModellsDao;
    public static RefernceModellsInformationsInformationsVisualisationsDao mivDao;
    public static InformationsVisualisationsDao informationsVisualisationsDao;
    public static InformationsDao informationsDao;
    public static ObjectActionZonesDao objectActionZonesDao;

    private DaoManager(Context context) {
        ConnectionSource connection = DBHelper.create(context).getConnectionSource();
        toursDao = ToursDao.createDao(connection);
        objectsDao = ObjectsDao.createDao(connection);
        refernceModellsDao = RefernceModellsDao.createDao(connection);
        mivDao = RefernceModellsInformationsInformationsVisualisationsDao.createDao(connection);
        informationsVisualisationsDao = InformationsVisualisationsDao.createDao(connection);
        informationsDao = InformationsDao.createDao(connection);
        objectActionZonesDao = ObjectActionZonesDao.createDao(connection);
    }

    private static DaoManager create(Context context) {
        return new DaoManager(context);
    }

    public static void init(Context context) {
        create(context);
    }
}
