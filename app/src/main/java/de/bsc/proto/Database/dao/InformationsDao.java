package de.bsc.proto.Database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import de.bsc.proto.Database.Modell.Informations;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class InformationsDao extends RuntimeExceptionDao<Informations, Integer> {
    static InformationsDao createDao(ConnectionSource connectionSource) {
        try {
            return new InformationsDao(DaoManager.<Dao<Informations, Integer>, Informations>createDao(connectionSource, Informations.class));
        } catch (SQLException e) {
            return null;
        }
    }

    public InformationsDao(Dao<Informations, Integer> dao) {
        super(dao);
    }

    public Informations getById(int id) {
        Informations t = null;
        try {
            t = getByIdQuery(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private Informations getByIdQuery(int id) throws SQLException {
        QueryBuilder<Informations, Integer> qb = queryBuilder();
        qb.where().eq(Informations.ID, id);
        return qb.queryForFirst();
    }
}
