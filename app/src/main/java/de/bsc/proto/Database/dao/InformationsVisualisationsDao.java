package de.bsc.proto.Database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import de.bsc.proto.Database.Modell.InformationsVisualisations;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class InformationsVisualisationsDao extends RuntimeExceptionDao<InformationsVisualisations, Integer> {
    static InformationsVisualisationsDao createDao(ConnectionSource connectionSource) {
        try {
            return new InformationsVisualisationsDao(DaoManager.<Dao<InformationsVisualisations, Integer>, InformationsVisualisations>createDao(connectionSource, InformationsVisualisations.class));
        } catch (SQLException e) {
            return null;
        }
    }

    public InformationsVisualisationsDao(Dao<InformationsVisualisations, Integer> dao) {
        super(dao);
    }

    public InformationsVisualisations getById(int id) {
        InformationsVisualisations t = null;
        try {
            t = getByIdQuery(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private InformationsVisualisations getByIdQuery(int id) throws SQLException {
        QueryBuilder<InformationsVisualisations, Integer> qb = queryBuilder();
        qb.where().eq(InformationsVisualisations.ID, id);
        return qb.queryForFirst();
    }
}
