package de.bsc.proto.Database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import de.bsc.proto.Database.Modell.ObjectActionZones;
import de.bsc.proto.Database.Modell.Objects;

/**
 * Created by RobNoFlop on 21/10/2017.
 */

public class ObjectActionZonesDao extends RuntimeExceptionDao<ObjectActionZones, Integer> {
    static ObjectActionZonesDao createDao(ConnectionSource connectionSource) {
        try {
            return new ObjectActionZonesDao(DaoManager.<Dao<ObjectActionZones, Integer>, ObjectActionZones>createDao(connectionSource, ObjectActionZones.class));
        } catch (SQLException e) {
            return null;
        }
    }

    public ObjectActionZonesDao(Dao<ObjectActionZones, Integer> dao) {
        super(dao);
    }

    public ObjectActionZones getById(int id) {
        ObjectActionZones t = null;
        try {
            t = getByIdQuery(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private ObjectActionZones getByIdQuery(int id) throws SQLException {
        QueryBuilder<ObjectActionZones, Integer> qb = queryBuilder();
        qb.where().eq(ObjectActionZones.ID, id);
        return qb.queryForFirst();
    }

    public List<ObjectActionZones> getByTour(Objects object) {
        List<ObjectActionZones> t = null;
        try {
            t = getByObejctQuery(object);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private List<ObjectActionZones> getByObejctQuery(Objects object) throws SQLException {
        QueryBuilder<ObjectActionZones, Integer> qb = queryBuilder();
        qb.where().eq(ObjectActionZones.OBJECTS_REF, object.getId());
        return qb.query();
    }
}
