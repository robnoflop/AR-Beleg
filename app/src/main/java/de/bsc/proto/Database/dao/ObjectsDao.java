package de.bsc.proto.Database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import de.bsc.proto.Database.Modell.Objects;
import de.bsc.proto.Database.Modell.Tours;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class ObjectsDao extends RuntimeExceptionDao<Objects, Integer> {
    static ObjectsDao createDao(ConnectionSource connectionSource) {
        try {
            return new ObjectsDao(DaoManager.<Dao<Objects, Integer>, Objects>createDao(connectionSource, Objects.class));
        } catch (SQLException e) {
            return null;
        }
    }

    public ObjectsDao(Dao<Objects, Integer> dao) {
        super(dao);
    }

    public Objects getById(int id) {
        Objects t = null;
        try {
            t = getByIdQuery(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private Objects getByIdQuery(int id) throws SQLException {
        QueryBuilder<Objects, Integer> qb = queryBuilder();
        qb.where().eq(Objects.ID, id);
        return qb.queryForFirst();
    }

    public List<Objects> getByTour(Tours tour) {
        List<Objects> t = null;
        try {
            t = getByTourQuery(tour);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private List<Objects> getByTourQuery(Tours tour) throws SQLException {
        QueryBuilder<Objects, Integer> qb = queryBuilder();
        qb.where().eq(Objects.TOURREF, tour.getId());
        return qb.query();
    }
}
