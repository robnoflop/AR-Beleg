package de.bsc.proto.Database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import de.bsc.proto.Database.Modell.Objects;
import de.bsc.proto.Database.Modell.RefernceModells;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class RefernceModellsDao extends RuntimeExceptionDao<RefernceModells, Integer> {
    static RefernceModellsDao createDao(ConnectionSource connectionSource) {
        try {
            return new RefernceModellsDao(DaoManager.<Dao<RefernceModells, Integer>, RefernceModells>createDao(connectionSource, RefernceModells.class));
        } catch (SQLException e) {
            return null;
        }
    }

    public RefernceModellsDao(Dao<RefernceModells, Integer> dao) {
        super(dao);
    }

    public RefernceModells getById(int id) {
        RefernceModells t = null;
        try {
            t = getByIdQuery(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private RefernceModells getByIdQuery(int id) throws SQLException {
        QueryBuilder<RefernceModells, Integer> qb = queryBuilder();
        qb.where().eq(RefernceModells.ID, id);
        return qb.queryForFirst();
    }

    public List<RefernceModells> getRefernceModellsByObjects(Objects object) {
        List<RefernceModells> t = null;
        try {
            t = getRefernceModellsByObjectsQuery(object);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private List<RefernceModells> getRefernceModellsByObjectsQuery(Objects object) throws SQLException {
        QueryBuilder<RefernceModells, Integer> qb = queryBuilder();
        qb.where().eq(RefernceModells.OBJECTSREF, object.getId());
        return qb.query();
    }
}
