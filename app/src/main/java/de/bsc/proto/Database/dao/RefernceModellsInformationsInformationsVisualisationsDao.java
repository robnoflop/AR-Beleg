package de.bsc.proto.Database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import de.bsc.proto.Database.Modell.RefernceModells;
import de.bsc.proto.Database.Modell.RefernceModellsInformationsInformationsVisualisations;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class RefernceModellsInformationsInformationsVisualisationsDao extends RuntimeExceptionDao<RefernceModellsInformationsInformationsVisualisations, Integer> {
    static RefernceModellsInformationsInformationsVisualisationsDao createDao(ConnectionSource connectionSource) {
        try {
            return new RefernceModellsInformationsInformationsVisualisationsDao(DaoManager.<Dao<RefernceModellsInformationsInformationsVisualisations, Integer>, RefernceModellsInformationsInformationsVisualisations>createDao(connectionSource, RefernceModellsInformationsInformationsVisualisations.class));
        } catch (SQLException e) {
            return null;
        }
    }

    public RefernceModellsInformationsInformationsVisualisationsDao(Dao<RefernceModellsInformationsInformationsVisualisations, Integer> dao) {
        super(dao);
    }

    public List<RefernceModellsInformationsInformationsVisualisations> getMIVByRefernceModells(RefernceModells modells) {
        List<RefernceModellsInformationsInformationsVisualisations> t = null;
        try {
            t = getMIVByRefernceModellsQuery(modells);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private List<RefernceModellsInformationsInformationsVisualisations> getMIVByRefernceModellsQuery(RefernceModells modells) throws SQLException {
        QueryBuilder<RefernceModellsInformationsInformationsVisualisations, Integer> qb = queryBuilder();
        qb.where().eq(RefernceModellsInformationsInformationsVisualisations.REFERENCE_MODELL_REF, modells.getId());
        return qb.query();
    }
}
