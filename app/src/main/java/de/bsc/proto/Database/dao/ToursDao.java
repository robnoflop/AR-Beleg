package de.bsc.proto.Database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import de.bsc.proto.Database.Modell.Tours;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class ToursDao extends RuntimeExceptionDao<Tours, Integer> {

    static ToursDao createDao(ConnectionSource connectionSource) {
        try {
            return new ToursDao(DaoManager.<Dao<Tours, Integer>, Tours>createDao(connectionSource, Tours.class));
        } catch (SQLException e) {
            return null;
        }
    }

    public ToursDao(Dao<Tours, Integer> dao) {
        super(dao);
    }

    public Tours getById(int id) {
        Tours t = null;
        try {
            t = getByIdQuery(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    private Tours getByIdQuery(int id) throws SQLException {
        QueryBuilder<Tours, Integer> qb = queryBuilder();
        qb.where().eq(Tours.ID, id);
        return qb.queryForFirst();
    }

    public List<Tours> getAll() {
        List<Tours> tours = null;
        try {
            tours = getAllQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tours;
    }

    private List<Tours> getAllQuery() throws SQLException {
        QueryBuilder<Tours, Integer> qb = queryBuilder();
        qb.where().gt(Tours.ID, 0);
        return qb.query();
    }
}
