package de.bsc.proto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import de.bsc.proto.Database.Modell.Tours;
import de.bsc.proto.Database.dao.DaoManager;

public class MainActivity extends AppCompatActivity {

    public static final String TOUR_INTENT_EXTRA = "TOUR_INTENT_EXTRA";

    private ListView lview;
    private List<Tours> tours;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tours = DaoManager.toursDao.getAll();

        lview = (ListView) findViewById(R.id.mainListView);
        lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                lViewOnClick(parent, view, position, id);
            }
        });
        ArrayAdapter<Tours> adapter = new ArrayAdapter<Tours>(this, android.R.layout.simple_list_item_1, tours);
        lview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSettingsClick(View v) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    public void lViewOnClick(AdapterView<?> parent, View view, int position, long id) {
        String name = ((TextView) view).getText().toString();
        for (Tours t : tours) {
            if (t.getName().equals(name)) {
                Intent intent = new Intent(MainActivity.this, TourMapsActivity.class);
                //Intent intent = new Intent(MainActivity.this, TourActivity.class);
                intent.putExtra(TOUR_INTENT_EXTRA, t);
                startActivity(intent);
                break;
            }
        }
    }
}
