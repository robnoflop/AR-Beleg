package de.bsc.proto;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import de.bsc.proto.Database.dao.DaoManager;

/**
 * Created by RobNoFlop on 24.12.2015.
 */
public class MyApplication extends Application {


    private static MyApplication instance;

    public static MyApplication app() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DaoManager.init(this);

        Logger.addLogAdapter(new AndroidLogAdapter());
        instance = this;
    }


}
