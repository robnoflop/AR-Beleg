package de.bsc.proto;

import android.app.Activity;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.InstallCallbackInterface;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import de.bsc.proto.Database.Modell.Tours;
import de.bsc.proto.Tracker.Tracker;
import de.bsc.proto.Views.ExtentedJavaCameraView;

//public class MainActivity extends AppCompatActivity implements CvCameraViewListener2 {
public class TourActivity extends Activity {

    private ExtentedJavaCameraView mOpenCvCameraView;

    private Tracker tracker;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tour);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mOpenCvCameraView = (ExtentedJavaCameraView) findViewById(R.id.cameraView);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

        Tours tour = (Tours) getIntent().getSerializableExtra(MainActivity.TOUR_INTENT_EXTRA);
        tracker = new Tracker(tour, mOpenCvCameraView, this);
        mOpenCvCameraView.setOnTouchListener(tracker);
        mOpenCvCameraView.setCvCameraViewListener(tracker);

    }

    @Override
    public void onResume() {
        super.onResume();
        loadOpenCV();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadOpenCV() {
        if (!OpenCVLoader.initDebug()) {
            Log.e("MY OPEN CV TEST", "opencv not loaded.");
        } else {
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }
}
