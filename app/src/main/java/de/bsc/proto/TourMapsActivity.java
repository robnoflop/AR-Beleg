package de.bsc.proto;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.f2prateek.rx.preferences2.Preference;
import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bsc.proto.Database.Modell.GpsLoaction;
import de.bsc.proto.Database.Modell.ObjectActionZones;
import de.bsc.proto.Database.Modell.Objects;
import de.bsc.proto.Database.Modell.Tours;
import de.bsc.proto.Database.dao.DaoManager;
import de.bsc.proto.utils.HttpConnection;
import de.bsc.proto.utils.PathJSONParser;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;

import static de.bsc.proto.MainActivity.TOUR_INTENT_EXTRA;

public class TourMapsActivity extends FragmentActivity implements OnMapReadyCallback, OnLocationUpdatedListener {

    private static final int DEFAULT_ZOOM = 13;

    private GoogleMap mMap;

    private boolean hasPosition = false;
    private Tours tour;
    private List<Objects> tourObjects;
    private Button augmentObject;

    private RxSharedPreferences rxPreferences;
    Preference<Boolean> isShowObjectRanges;
    Preference<Boolean> isAutoAugment;
    Preference<Boolean> isOjectNearbyNotify;

    Map<Objects, Polygon> objectsPolygonMap = new HashMap<>();
    boolean hasVisibleZoneEnterted = false; //Todo save this in state and load state

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_maps);

        tour = (Tours) getIntent().getSerializableExtra(TOUR_INTENT_EXTRA);
        tourObjects = DaoManager.objectsDao.getByTour(tour);
        augmentObject = (Button) findViewById(R.id.augment_object);
        activetButton(false);
        augmentObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (augmentObject.isActivated()) {
                    Intent intent = new Intent(getApplicationContext(), TourActivity.class); //ToDo isAutoAugment
                    intent.putExtra(TOUR_INTENT_EXTRA, tour);
                    startActivity(intent);
                }
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initPrefListener();
    }

    private void initPrefListener() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        rxPreferences = RxSharedPreferences.create(preferences);


        String key = getString(R.string.pref_tour_show_object_ranges_key);
        String def = getString(R.string.pref_tour_show_object_ranges_default);
        isShowObjectRanges = rxPreferences.getBoolean(key, Boolean.valueOf(def));

        key = getString(R.string.pref_tour_auto_augment_key);
        def = getString(R.string.pref_tour_auto_augment_default);
        isAutoAugment = rxPreferences.getBoolean(key, Boolean.valueOf(def));

        key = getString(R.string.pref_tour_object_nearby_notify_key);
        def = getString(R.string.pref_tour_object_nearby_notify_default);
        isOjectNearbyNotify = rxPreferences.getBoolean(key, Boolean.valueOf(def));
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        float trackingDistance = 5;
        LocationAccuracy trackingAccuracy = LocationAccuracy.HIGH;

        LocationParams.Builder builder = new LocationParams.Builder()
                .setAccuracy(trackingAccuracy)
                .setDistance(trackingDistance);
        SmartLocation.with(this).location().continuous().config(builder.build()).start(this);
        //SmartLocation.with(this).location().continuous().config(LocationParams.LAZY).start(this);

        loadTour();
    }

    @Override
    public void onLocationUpdated(Location location) {
        //if (location.isFromMockProvider()) {
            if (hasPosition) {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(
                        new LatLng(location.getLatitude(),
                                location.getLongitude())));
            } else {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(location.getLatitude(),
                                location.getLongitude()), DEFAULT_ZOOM));
                hasPosition = true;
            }
            doTourObjectAction(location);
        //}
    }


    private void loadTour() {
        List<LatLng> positions = new ArrayList<>();
        for (Objects o : tourObjects) {
            LatLng position = new LatLng(o.getLatitude(), o.getLongitude());
            positions.add(position);
            mMap.addMarker(new MarkerOptions()
                    .title(o.getName())
                    .position(position));

            List<ObjectActionZones> zones = DaoManager.objectActionZonesDao.getByTour(o);
            List<GpsLoaction> locations = zones.get(0).getPointsList();
            PolygonOptions visibleShape = new PolygonOptions();
            for (GpsLoaction l: locations) {
                visibleShape.add(new LatLng(l.latitude, l.longitude));
            }
            if(!isShowObjectRanges.get()) {
                visibleShape.visible(false);
            }
            Polygon polygon = mMap.addPolygon(visibleShape);
            objectsPolygonMap.put(o, polygon);
        }
        String url = getMapsApiDirectionsUrl(positions);
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);
    }

    private String getMapsApiDirectionsUrl(List<LatLng> positions) {
        String wayPointsString = "";
        for (LatLng position : positions) {
            wayPointsString += "|" + position.latitude + "," + position.longitude;
        }

        String origin = "origin=" + positions.get(0).latitude + "," + positions.get(0).longitude;
        String destination = "destination=" + positions.get(positions.size() - 1).latitude + "," +
                positions.get(positions.size() - 1).longitude;
        String waypoints = "waypoints=optimize:true" + wayPointsString;
        String mode = "mode=walking";
        String sensor = "sensor=false";
        String params = origin + "&" + destination + "&" + waypoints + "&" + mode + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + params;
        return url;
    }

    private void activetButton(boolean activate) {
        if (activate) {
            augmentObject.getBackground().setTint(getColor(R.color.colorAccent));
            augmentObject.setActivated(true);
        } else {
            augmentObject.getBackground().setTint(getColor(R.color.inactiveButtonBackground));
            augmentObject.setActivated(false);
        }
    }

    private void doTourObjectAction(Location location) {
        int visibleCount = 0;
        for (Objects o : tourObjects) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            Boolean isInVisibleRange = PolyUtil.containsLocation(latLng, objectsPolygonMap.get(o).getPoints(), false);

            visibleCount += isInVisibleRange ? 1 : 0;

            if(isInVisibleRange && !hasVisibleZoneEnterted) {
                hasVisibleZoneEnterted = true;
                if(isOjectNearbyNotify.get()) {
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    v.vibrate(500);
                }

                activetButton(true);
                break;
            }
        }

        if (visibleCount == 0) {
            activetButton(false);
            hasVisibleZoneEnterted = false;
        }

    }

    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                //polyLineOptions.width(2);
                polyLineOptions.color(Color.BLUE);
            }

            mMap.addPolyline(polyLineOptions);
        }
    }
}
