package de.bsc.proto.Tracker.Detection;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import java.util.ArrayList;

/**
 * Created by RobNoFlop on 17.01.2016.
 */
public class BaseMatcher {

    protected ContourMatchingDescriptor contourMatchingDescriptor;

    protected Mat image;

    protected Point pos;

    protected Rect boundingRec;

    protected ArrayList<Point> contour;

    protected Mat transformMatrix;

    protected boolean error = false;

    protected boolean match = false;

    public ContourMatchingDescriptor getContourMatchingDescriptor() {
        return contourMatchingDescriptor;
    }

    public Mat getResultImage() {
        return image.clone();
    }

    public Point getPos() {
        return pos;
    }

    public Rect getBoundingRec() {
        return boundingRec;
    }

    public ArrayList<Point> getContour() {
        return contour;
    }

    public Mat getTransformMatrix() {
        return transformMatrix;
    }
}
