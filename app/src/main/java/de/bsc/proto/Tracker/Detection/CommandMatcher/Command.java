package de.bsc.proto.Tracker.Detection.CommandMatcher;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RobNoFlop on 22.12.2015.
 */
public class Command {
    public String name;
    public List<String> params = new ArrayList<>();
}
