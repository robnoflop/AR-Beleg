package de.bsc.proto.Tracker.Detection.CommandMatcher;

import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.bsc.proto.Tracker.Detection.BaseMatcher;
import de.bsc.proto.Tracker.Detection.ContourMatchingDescriptor;

import static org.opencv.calib3d.Calib3d.findHomography;

/**
 * Created by RobNoFlop on 23.12.2015.
 */
public class CommandBasedMatcher extends BaseMatcher {

    private Mat helper = new Mat();

    private List<MatOfPoint> objectContours;

    public List<MatOfPoint> getObjectContours() {
        return objectContours;
    }

    public boolean match(DetectorObjectDescription decriptor, Mat image, ContourMatchingDescriptor refContourMatchingDescriptor) {
        this.image = image;
        error = false;
        match = false;

        try {
            tryMatch(decriptor, refContourMatchingDescriptor);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return match;
    }

    private void tryMatch(DetectorObjectDescription decriptor, ContourMatchingDescriptor refContourMatchingDescriptor) {
        bgra2bgr();
        for (Command c : decriptor.getCommands()) {
            if (c.name.equalsIgnoreCase("rgbtohsv")) {
                bgr2hsv();
            } else if (c.name.equalsIgnoreCase("hsvFilter")) {
                hsvFilter(c.params);
            } else if (c.name.equalsIgnoreCase("contour")) {
                contour(c.params);
            } else if (c.name.equalsIgnoreCase("findCenter")) {
                findCenter(c.params);
            } else if (c.name.equalsIgnoreCase("matchMaxShape")) {
                matchMaxShape(c.params, refContourMatchingDescriptor);
            } else if (c.name.equalsIgnoreCase("crop")) {
                crop();
            } else if (c.name.equalsIgnoreCase("shap") || c.name.equalsIgnoreCase("sharp")) {
                sharp();
            } else if (c.name.equalsIgnoreCase("leftRightFloodFill")) {
                leftRightFloodFill();
            } else if (c.name.equalsIgnoreCase("rgbtogray")) {
                bgr2gray();
            } else if (c.name.equalsIgnoreCase("thres")) {
                thres(c.params);
            } else if (c.name.equalsIgnoreCase("opening")) {
                opening(c.params);
            } else if (c.name.equalsIgnoreCase("closing")) {
                closing(c.params);
            }
            if (error) {
                break;
            }
        }
    }

    private void matchMaxShape(List<String> parmas, ContourMatchingDescriptor refContourMatchingDescriptor) {
        List<MatOfPoint> foundContour = contour2(true);
        if (!error) {
            parmas.set(0, parmas.get(0).replace('[', ' '));
            parmas.set(0, parmas.get(0).replace(']', ' '));

            String[] points = parmas.get(0).split("[;]");
            List<Point> counturPoints = new ArrayList<>();
            for (String point : points) {
                String[] p = point.split("[,]");
                counturPoints.add(new Point(Integer.parseInt(p[0].trim()), Integer.parseInt(p[1].trim())));
            }

            MatOfPoint refContour = new MatOfPoint();
            refContour.fromList(counturPoints);

            double similarity = Imgproc.matchShapes(refContour, foundContour.get(0), Imgproc.CV_CONTOURS_MATCH_I3, 0);

            MatOfPoint2f m = new MatOfPoint2f(foundContour.get(0).toArray());

            contourMatchingDescriptor = ContourMatchingDescriptor.buildFromContour(m, image.cols(), image.rows());

            MatOfPoint2f c = new MatOfPoint2f();
            Imgproc.approxPolyDP(m, c, 15, true);
            MatOfPoint contour = new MatOfPoint(c.toArray());

            double refSimilarity = 5;
            if (parmas.size() > 1) {
                refSimilarity = Double.parseDouble(parmas.get(1));
            }

            if (similarity < refSimilarity) {
                match = true;
                pos = findCenterPoint(contour);
                boundingRec = Imgproc.boundingRect(contour);
                List<MatOfPoint> help = new ArrayList<>();
                help.add(contour);
                objectContours = help;
                findTransformdContour(contourMatchingDescriptor, refContourMatchingDescriptor);
            }
        }
    }

    private void findTransformdContour(ContourMatchingDescriptor contourMatchingDescriptor,
                                ContourMatchingDescriptor refContourMatchingDescriptor) {

        MatOfPoint2f matchedPointsMat = new MatOfPoint2f();
        matchedPointsMat.fromList(contourMatchingDescriptor.getMatchingPoints());

        MatOfPoint2f matchedRefPointsMat = new MatOfPoint2f();
        matchedRefPointsMat.fromList(refContourMatchingDescriptor.getMatchingPoints());

        Mat tranformMatrix = findHomography(matchedRefPointsMat, matchedPointsMat);

        List<Point> contour = refContourMatchingDescriptor.getContour().toList();
        Mat transformed = new Mat();
        ArrayList<Point> trasContour = new ArrayList<>();
        Core.perspectiveTransform(Converters.vector_Point2f_to_Mat(contour), transformed, tranformMatrix);
        Converters.Mat_to_vector_Point2f(transformed, trasContour);

        this.contour = trasContour;
    }

    private Point findCenterPoint(MatOfPoint contour) {
        Moments m = Imgproc.moments(contour, false);
        int x = (int) (m.get_m10() / m.get_m00());
        int y = (int) (m.get_m01() / m.get_m00());
        return new Point(x, y);
    }

    private void bgr2hsv() {
        Imgproc.cvtColor(image, helper, Imgproc.COLOR_BGR2HSV_FULL);
        image = helper;
    }

    private void bgr2gray() {
        Imgproc.cvtColor(image, helper, Imgproc.COLOR_BGR2GRAY);
        image = helper;
    }

    private void bgra2bgr() {
        Imgproc.cvtColor(image, helper, Imgproc.COLOR_BGRA2BGR);
        image = helper;
    }

    private void bgr2bgra() {
        Imgproc.cvtColor(image, helper, Imgproc.COLOR_BGR2BGRA);
        image = helper;
    }

    private void hsvFilter(List<String> params) {
        Core.inRange(image,
                new Scalar((double) Integer.parseInt(params.get(0)),
                        (double) Integer.parseInt(params.get(1)),
                        (double) Integer.parseInt(params.get(2))),
                new Scalar((double) Integer.parseInt(params.get(3)),
                        (double) Integer.parseInt(params.get(4)),
                        (double) Integer.parseInt(params.get(5))),
                helper);
        image = helper;
    }

    private List<MatOfPoint> contour2(boolean max) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(image, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        if (contours.size() == 0) {
            error = true;
            return null;
        }

        if (max) {
            int index = 0;
            double maxsize = 0;
            for (int i = 0; i < contours.size(); i++) {
                double size = Imgproc.contourArea(contours.get(i));
                if (size > maxsize) {
                    index = i;
                    maxsize = size;
                }
            }

            Imgproc.drawContours(helper, contours, index, new Scalar(255, 255, 255), -1, 8, hierarchy, 0, new Point());

            List<MatOfPoint> c = new ArrayList<>();
            c.add(contours.get(index));
            contours = c;

        } else {
            Random rng = new Random();
            for (int i = 0; i < contours.size(); i++) {
                Scalar color = new Scalar(rng.nextInt(255), rng.nextInt(255), rng.nextInt(255));
                Imgproc.drawContours(helper, contours, i, color, 2, 8, hierarchy, 0, new Point());
            }

        }
        image = helper;

        return contours;
    }

    private List<MatOfPoint> contour(List<String> params) {
        boolean max = params.get(0).equals("1") ? true : false;
        return contour2(max);
    }

    private void gaus(int size) {
        Imgproc.GaussianBlur(image, image, new Size(size, size), 0, 0);
    }

    private void findCenter(List<String> params) {
        params.set(0, params.get(0).equals("0") ? "1" : "0");
        List<MatOfPoint> contours = contour(params);

        if (!error) {
            List<Moments> mu = new ArrayList<>();
            for (int i = 0; i < contours.size(); i++) {
                try {
                    mu.add(Imgproc.moments((Mat) contours.get(i), false));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            ///  Get the mass centers:
            List<Point> mc = new ArrayList<>();
            for (int i = 0; i < contours.size(); i++) {
                mc.add(new Point(mu.get(i).m10 / mu.get(i).m00, mu.get(i).m01 / mu.get(i).m00));
            }

            if (mc.size() > 0) {
                match = true;
                pos = mc.get(0);
            }
        }
    }

    private void crop() {
        int x1 = image.cols() / 3;
        int y1 = 0;
        int x2 = image.cols() / 3;
        int y2 = image.rows() / 3 * 2;
        image = image.adjustROI(x1, y1, x2, y2);
    }

    private void sharp() {
        Mat temp = new Mat();
        gaus(3);
        Mat kern = new Mat(3, 3, CvType.CV_8SC1);
        int[][] k = {{-1, -1, -1},
                {-1, 9, -1},
                {-1, -1, -1}};
        for (int i = 0; i < k.length; i++) {
            for (int j = 0; j < k[i].length; j++) {
                kern.put(i, j, k[i][j]);
            }
        }

        Imgproc.filter2D(image, temp, image.depth(), kern);
        image = temp.clone();
        temp.release();
        kern.release();
    }

    private void leftRightFloodFill() {
        int colls = image.cols();
        for (int i = 0; i < image.cols(); i = i + 200) {
            Scalar border = new Scalar(8, 8, 8);
            Imgproc.floodFill(image, new Mat(), new Point(i, 1), new Scalar(255, 255, 255), new Rect(), border, border, 8);
            Imgproc.floodFill(image, new Mat(), new Point(i, image.rows() - 1), new Scalar(255, 255, 255), new Rect(), border, border, 8);
        }
    }

    private void thres(List<String> params) {
        Imgproc.threshold(image, helper, Integer.parseInt(params.get(0)), Integer.parseInt(params.get(1)), Integer.parseInt(params.get(2)));
        image = helper;
    }

    private void closing(List<String> params) {
        int amount = Integer.parseInt(params.get(0));
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
        for (int i = 0; i < amount; i++) {
            Imgproc.dilate(image, helper, element);
            image = helper;
        }
        for (int i = 0; i < amount; i++) {
            Imgproc.erode(image, helper, element);
            image = helper;
        }
    }

    private void opening(List<String> params) {
        int amount = Integer.parseInt(params.get(0));
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
        for (int i = 0; i < amount; i++) {
            Imgproc.erode(image, helper, element);
            image = helper;
        }
        for (int i = 0; i < amount; i++) {
            Imgproc.dilate(image, helper, element);
            image = helper;
        }
    }
}
