package de.bsc.proto.Tracker.Detection.CommandMatcher;

import android.util.Xml;

import org.opencv.core.Scalar;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.bsc.proto.Tracker.Detection.CommandMatcher.Command;

/**
 * Created by RobNoFlop on 22.12.2015.
 */
public class DetectorObjectDescription {

    public enum SURFACES {SMOOTH, ABRASIVE, PATTERNED, STRIPED}

    public enum SIZES {HUGE, SMALL, NORMAL}

    public enum SHAPES {ROUND, SQUARE, OVAL, LONG, SHORT}

    private List<Command> commands;

    private Scalar resultColor;

    private List<SHAPES> shapes;

    private List<SIZES> sizes;

    private List<SURFACES> surfaces;

    private Scalar mainColor;

    public List<Command> getCommands() {
        return commands;
    }

    public Scalar getResultColor() {
        return resultColor;
    }

    public List<SHAPES> getShapes() {
        return shapes;
    }

    public List<SIZES> getSizes() {
        return sizes;
    }

    public List<SURFACES> getSurfaces() {
        return surfaces;
    }

    public Scalar getMainColor() {
        return mainColor;
    }

    public int getNumberOfHoles() {
        return numberOfHoles;
    }

    private int numberOfHoles;

    public DetectorObjectDescription(String xmlObjectDescription) throws IOException {
        commands = new ArrayList<>();
        shapes = new ArrayList<>();
        sizes = new ArrayList<>();
        surfaces = new ArrayList<>();
        parseXML(xmlObjectDescription);
    }

    private void parseXML(String xml) throws IOException {
        InputStream in = new ByteArrayInputStream(xml.getBytes());

        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            readFeed(parser);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            in.close();
        }
    }

    private void readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, null, "object");
        Command c = null;
        boolean commandStart = false;
        boolean run = true;

        while (run) {
            parser.nextTag();
            String name = parser.getName();
            if (parser.getEventType() == XmlPullParser.START_TAG) {
                // Starts by looking for the entry tag
                if (name.equals("command")) {
                    c = new Command();
                    c.name = parser.getAttributeValue(0);
                    commandStart = true;
                } else if (name.equals("param") && commandStart) {
                    c.params.add(parser.getAttributeValue(0));
                } else if (name.equals("resultColor") || name.equals("mainColor")) {
                    try {
                        int r = Integer.parseInt(parser.getAttributeValue(0));
                        int g = Integer.parseInt(parser.getAttributeValue(1));
                        int b = Integer.parseInt(parser.getAttributeValue(2));
                        if (name.equals("resultColor"))
                            resultColor = new Scalar((double) r, (double) g, (double) b);

                        if (name.equals("mainColor"))
                            mainColor = new Scalar((double) r, (double) g, (double) b);
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                } else if (name.equals("shape") || name.equals("size") || name.equals("surface")) {
                    String[] values = parser.getAttributeValue(0).split("[|]");
                    for (String s : values) {
                        try {
                            if (name.equals("shape"))
                                shapes.add(SHAPES.valueOf(s.toUpperCase()));

                            if (name.equals("size"))
                                sizes.add(SIZES.valueOf(s.toUpperCase()));

                            if (name.equals("surface"))
                                surfaces.add(SURFACES.valueOf(s.toUpperCase()));
                        } catch (IllegalArgumentException ex) {
                            ex.printStackTrace();
                        }
                    }
                } else if (name.equals("numberOfHoles")) {
                    try {
                        numberOfHoles = Integer.parseInt(parser.getAttributeValue(0));
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                }
            } else if (parser.getEventType() == XmlPullParser.END_TAG) {
                if (name.equals("command")) {
                    commands.add(c);
                    commandStart = false;
                } else if (name.equals("object")) {
                    run = false;
                }
            }
        }
    }

}
