package de.bsc.proto.Tracker.Detection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.KeyPoint;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RobNoFlop on 17/12/2017.
 */

public class ContourMatchingDescriptor {

    private ArrayList<Point> matchingPoints;
    private ArrayList<Point> matchingPointsDescriptions = new ArrayList<>();
    private MatOfPoint2f contour;
    private Point massCenter;
    private Point top;
    private int xScale;
    private int yScale;

    public static ContourMatchingDescriptor buildFromContour(MatOfPoint2f contour, int cols, int rows) {
        ArrayList<Point> matchingPoints = new ArrayList<>();

        //find heeghst contour point
        double minY = 1000000;
        int minYindex = 0;
        List<Point> contourList = contour.toList();
        for (int i = 0; i < contourList.size(); i++) {
            if (contourList.get(i).y < minY) {
                minYindex = i;
                minY = contourList.get(i).y;
            }
        }

        Point maxPoint = contourList.get(minYindex);
        matchingPoints.add(maxPoint);

        /// Get the moments
        Moments mu = Imgproc.moments(contour, false);
        Point massCenter = new Point(mu.m10 / mu.m00, mu.m01 / mu.m00);
        matchingPoints.add(massCenter);

        MatOfPoint intContour = new MatOfPoint(contour.toArray());
        Rect boundRect = Imgproc.boundingRect(intContour);
        matchingPoints.add(new Point(boundRect.x, boundRect.y));
        matchingPoints.add(new Point(boundRect.x + boundRect.width, boundRect.y));
        matchingPoints.add(new Point(boundRect.x, boundRect.y + boundRect.height));
        matchingPoints.add(new Point(boundRect.x + boundRect.width, boundRect.y + boundRect.height));

        RotatedRect rec = Imgproc.fitEllipse(contour);
        boundRect = rec.boundingRect();
        matchingPoints.add(rec.center);
        matchingPoints.add(new Point(boundRect.x, boundRect.y));
        matchingPoints.add(new Point(boundRect.x + boundRect.width, boundRect.y));
        matchingPoints.add(new Point(boundRect.x, boundRect.y + boundRect.height));
        matchingPoints.add(new Point(boundRect.x + boundRect.width, boundRect.y + boundRect.height));

        rec = Imgproc.minAreaRect(contour);
        boundRect = rec.boundingRect();
        matchingPoints.add(rec.center);
        matchingPoints.add(new Point(boundRect.x, boundRect.y));
        matchingPoints.add(new Point(boundRect.x + boundRect.width, boundRect.y));
        matchingPoints.add(new Point(boundRect.x, boundRect.y + boundRect.height));
        matchingPoints.add(new Point(boundRect.x + boundRect.width, boundRect.y + boundRect.height));

        Point center = new Point();
        float[] r = new float[1];
        Imgproc.minEnclosingCircle(contour, center, r);
        matchingPoints.add(center);

        return new ContourMatchingDescriptor(matchingPoints, contour, cols, rows);
    }

    public static ContourMatchingDescriptor buildFromKeyPoints(MatOfPoint2f contour, int cols, int rows, MatOfKeyPoint keyPoints) {
        ArrayList<Point> matchingPoints = new ArrayList<>();
        for (KeyPoint point : keyPoints.toArray()) {
            matchingPoints.add(point.pt);
        }
        return new ContourMatchingDescriptor(matchingPoints, contour, cols, rows);
    }

    public static ContourMatchingDescriptor buildFromJson(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);

        ArrayList<Point> matchingPoints = new ArrayList<>();
        if(jsonObject.has("matchingPoints")) {
            JSONArray array = jsonObject.getJSONArray("matchingPoints");
            for (int i = 0; i < array.length(); i++) {
                int x = array.getJSONObject(i).getJSONArray("point").getInt(0);
                int y = array.getJSONObject(i).getJSONArray("point").getInt(1);
                matchingPoints.add(new Point(x, y));
            }
        }

        ArrayList<Point> contourList = new ArrayList<>();
        if(jsonObject.has("contour")) {
            JSONArray array = jsonObject.getJSONArray("contour");
            for (int i = 0; i < array.length(); i++) {
                int x = array.getJSONObject(i).getJSONArray("point").getInt(0);
                int y = array.getJSONObject(i).getJSONArray("point").getInt(1);
                contourList.add(new Point(x, y));
            }
        }
        MatOfPoint2f contour = new MatOfPoint2f();
        contour.fromList(contourList);

        int xScale = jsonObject.getInt("xScale");
        int yScale = jsonObject.getInt("yScale");

        return new ContourMatchingDescriptor(matchingPoints, contour, xScale, yScale);
    }


    private int getDistance(Point p1, Point p2) {
        return (int) Math.abs((p1.x - p2.x) + (p1.y - p2.y));
    }

    //public ContourMatchingDescriptor(ArrayList<Point> matchingPoints, ArrayList<Point> refContour, int xScale, int yScale) {


    public ContourMatchingDescriptor(ArrayList<Point> matchingPoints, MatOfPoint2f contour, int xScale, int yScale) {
        this.matchingPoints = matchingPoints;
        this.contour = contour;
        this.xScale = xScale;
        this.yScale = yScale;
        createMatchingPointDescription();
    }

    private void createMatchingPointDescription() {
        for (Point p : matchingPoints) {
            matchingPointsDescriptions.add(scalePointDown(p));
        }
    }

    public ArrayList<Point> getMatchingPoints() {
        return matchingPoints;
    }

    public ArrayList<Point> getMatchingPointsDescriptions() {
        return matchingPointsDescriptions;
    }

    public MatOfPoint2f getContour() {
        return contour;
    }

    public Point scalePointDown(Point p) {
        return new Point(p.x / xScale, p.y / yScale);
    }

    public Point scalePointUp(Point p) {
        return new Point(p.x * xScale, p.y * yScale);
    }

    //void writeDescriptor(String fileName);

    ArrayList<Point> translateReferencePointsForCenter(ContourMatchingDescriptor refDescriptor) {
        ArrayList<Point> translatedRefPoints = new ArrayList<>();
        for (Point p : refDescriptor.getMatchingPointsDescriptions()) {
            translatedRefPoints.add(scalePointUp(p));
        }
        return translatedRefPoints;
    }

}
