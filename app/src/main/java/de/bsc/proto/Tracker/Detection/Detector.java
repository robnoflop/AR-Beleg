package de.bsc.proto.Tracker.Detection;

import android.content.Context;
import android.hardware.SensorManager;
import android.location.Location;

import org.json.JSONException;
import org.opencv.core.Mat;
import org.opencv.core.Point;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bsc.proto.Database.Modell.GpsLoaction;
import de.bsc.proto.Database.Modell.ObjectActionZones;
import de.bsc.proto.Database.Modell.Objects;
import de.bsc.proto.Database.Modell.RefernceModells;
import de.bsc.proto.Database.Modell.Tours;
import de.bsc.proto.Database.dao.DaoManager;
import de.bsc.proto.Tracker.Detection.CommandMatcher.CommandBasedMatcher;
import de.bsc.proto.Tracker.Detection.CommandMatcher.DetectorObjectDescription;
import de.bsc.proto.Tracker.Detection.FeatureMacher.FeaturePointBasedMatcher;
import de.bsc.proto.Tracker.Interaction.ILocationSubscriber;
import de.bsc.proto.Tracker.Interaction.IOrientationSubscriber;
import de.bsc.proto.Tracker.Interaction.SensorManagement;
import de.bsc.proto.Tracker.Tracking.Follower;
import de.bsc.proto.Tracker.Tracking.TemplateFollower;

import static org.opencv.calib3d.Calib3d.findHomography;

/**
 * Created by RobNoFlop on 20.11.2015.
 */
public class Detector implements ILocationSubscriber, IOrientationSubscriber {

    private Location currentLocation;

    private int currentOrientation;

    private Mat currentFrame;

    private Point pose;

    private RefernceModells detectedRefernce;

    private SensorManagement sensorManagement;

    private List<Objects> tourObjects;

    private Map<Objects, List<RefernceModells>> refernceModellsMap = new HashMap<>();

    private List<RefernceModells> filteredRefernceModells = new ArrayList<>();

    private Map<RefernceModells, ContourMatchingDescriptor> contourMatchingDescriptorMap = new HashMap<>();

    private DetectorResult result;

    private DetectorResult newResut = new DetectorResult();

    private Runnable performDetection = new Runnable() {
        @Override
        public void run() {
            performDetection();
        }
    };

    private Thread performDetectionThread = null;

    private Context context;

    private Follower follower;

    private boolean debug = false;

    public Detector(Context context, Tours tour) {
        sensorManagement = new SensorManagement((SensorManager) context.getSystemService(Context.SENSOR_SERVICE));
        sensorManagement.subscribe((ILocationSubscriber) this);
        sensorManagement.subscribe((IOrientationSubscriber) this);
        tourObjects = DaoManager.objectsDao.getByTour(tour);
        this.context = context;
    }

    public DetectorResult getResult() {
        DetectorResult r = null;
        if (result != null) {
            r = new DetectorResult(result);
        }
        return r;
    }

    public void start() {
        sensorManagement.start(context);
    }

    public void stop() {
        sensorManagement.stop(context);
    }

    public void detect(Mat frames) {
        if (performDetectionThread == null || performDetectionThread.getState() == Thread.State.TERMINATED) {
            currentFrame = frames.clone();
            performDetectionThread = new Thread(performDetection);
            performDetectionThread.start();
        }
    }

    @Override
    public void loactionUpdate(Location newLoaction) {
        currentLocation = newLoaction;
        updateReferenzModells(extracktObjectsInArea());
        filterRefernceModells();
    }

    private void filterRefernceModells() {
        List<RefernceModells> filteredModells = new ArrayList<>();
        for (Map.Entry<Objects, List<RefernceModells>> obj : refernceModellsMap.entrySet()) {
            if (isObjectVisible(obj.getKey())) {
                for (RefernceModells modell : obj.getValue()) {
                    if (viewDistance(currentOrientation, modell.getDirection()) <= 90) {
                        filteredModells.add(modell);
                    }
                }
            }
        }
        filteredRefernceModells = filteredModells;
    }

    private int viewDistance(int a, int b) {
        int d = Math.abs(a - b) % 360;
        return d > 180 ? 360 - d : d;
    }

    private void updateReferenzModells(List<Objects> objectsInArea) {
        Map<Objects, List<RefernceModells>> newRefernceModellsMap = new HashMap<>();
        for (Objects obj : objectsInArea) {
            if (refernceModellsMap.containsKey(obj)) {
                newRefernceModellsMap.put(obj, refernceModellsMap.get(obj));
            } else {
                //List<RefernceModells> modells = new ArrayList<>();
                //modells.add(DaoManager.refernceModellsDao.getById(125)); //turm
                //modells.add(DaoManager.refernceModellsDao.getById(195)); //arena
                List<RefernceModells> modells = DaoManager.refernceModellsDao.getRefernceModellsByObjects(obj);
                newRefernceModellsMap.put(obj, modells);
            }
        }
        refernceModellsMap = newRefernceModellsMap;
    }

    private List<Objects> extracktObjectsInArea() {
        List<Objects> objectsInArea = new ArrayList<>();
        for (Objects obj : tourObjects) {
            List<ObjectActionZones> zones = DaoManager.objectActionZonesDao.getByTour(obj);
            if (zones.get(0).isLocationInZone(new GpsLoaction(currentLocation.getLatitude(), currentLocation.getLongitude()))) {
                objectsInArea.add(obj);
            }
        }
        return objectsInArea;
    }

    private boolean isObjectVisible(Objects obj) {
        int viewingAngel = Math.round(currentLocation.bearingTo(obj.getLocation()));

        boolean result = false;
        int help = viewDistance(viewingAngel, currentOrientation);
        if (help <= 90) {
            result = true;
        }
        result = true;
        return result;
    }

    @Override
    public void orientationUpdate(double newOrientation) {
        currentOrientation = (int) Math.round(newOrientation);
        filterRefernceModells();
    }

    private void performDetection() {
        try {
            if (follower != null && follower.isFollowring()) {
                follower.follow(currentFrame);
                if (follower.isFollowring()) {
                    result.setBoundingRec(follower.getBoundRect());
                    result.setPose(follower.getPose());
                    Mat tranformMatrix = follower.getTransformation();
                    result.setTransformationMat(tranformMatrix);
                } else {
                    result = null;
                }
            } else if (match()) {
                result = newResut;
                follower = new TemplateFollower(currentFrame, result.getBoundingRec());
            }
        } catch (Exception e) {
            e.printStackTrace();
            //reset all;
        }
    }

    private boolean match() {
        boolean match = false;
        for (RefernceModells modell : filteredRefernceModells) {
            if (!contourMatchingDescriptorMap.containsKey(modell)) {
                try {
                    ContourMatchingDescriptor descriptor = ContourMatchingDescriptor
                            .buildFromJson(modell.getCountourMatchingDescriptor());
                    contourMatchingDescriptorMap.put(modell, descriptor);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            switch (modell.getDetectionMethod()) {
                case "ORB":
                    match = performORBDetection(modell);
                    break;
                case "XML":
                    match = performXMLDetection(modell);
                    break;
            }

            if (match) {
                newResut.setDetectedRefernce(modell);
                break;
            }
        }
        return match;
    }

    private boolean performXMLDetection(RefernceModells modell) {
        boolean match = false;
        try {
            DetectorObjectDescription descriptor = new DetectorObjectDescription(modell.getModell());
            CommandBasedMatcher matcher = new CommandBasedMatcher();
            match = matcher.match(descriptor, currentFrame, contourMatchingDescriptorMap.get(modell));

            newResut.setPose(matcher.getPos());
            newResut.setBoundingRec(matcher.getBoundingRec());
            newResut.setObjectContours(matcher.getObjectContours());
            newResut.setContour(matcher.getContour());
            newResut.setTransformationMat(matcher.getTransformMatrix());
            if (debug) {
                match = true;
                newResut.setDebug(true);
                newResut.setDebugFrame(matcher.getResultImage());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return match;
    }

    private boolean performORBDetection(RefernceModells modell) {
        FeaturePointBasedMatcher matcher = new FeaturePointBasedMatcher();
        boolean match = matcher.performORBDetection(modell, currentFrame, contourMatchingDescriptorMap.get(modell));

        newResut.setPose(matcher.getPos());
        newResut.setBoundingRec(matcher.getBoundingRec());
        newResut.setContour(matcher.getContour());
        newResut.setTransformationMat(matcher.getTransformMatrix());
        if (debug) {
            match = true;
            newResut.setDebug(true);
            newResut.setDebugFrame(matcher.getResultImage());
        }

        return match;
    }
}
