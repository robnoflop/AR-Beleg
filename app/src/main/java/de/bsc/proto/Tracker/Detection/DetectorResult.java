package de.bsc.proto.Tracker.Detection;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import de.bsc.proto.Database.Modell.RefernceModells;

/**
 * Created by RobNoFlop on 26.12.2015.
 */
public class DetectorResult {
    private Point pose;
    private RefernceModells detectedRefernce;
    private Mat debugFrame;
    private boolean debug;
    private Rect boundingRec;
    private List<Point> contour;
    private List<MatOfPoint> objectContours;
    private Mat transformationMat;

    private ReadWriteLock poseLock = new ReentrantReadWriteLock();
    private ReadWriteLock detectedRefernceLock = new ReentrantReadWriteLock();
    private ReadWriteLock debugFrameLock = new ReentrantReadWriteLock();
    private ReadWriteLock debugLock = new ReentrantReadWriteLock();
    private ReadWriteLock boundingRecLock = new ReentrantReadWriteLock();
    private ReadWriteLock objectContoursLock = new ReentrantReadWriteLock();
    private ReadWriteLock contourLock = new ReentrantReadWriteLock();
    private ReadWriteLock transformationLock = new ReentrantReadWriteLock();

    public DetectorResult() {

    }

    public DetectorResult(DetectorResult other) {
        if (other != null) {
            synchronized (other) {
                this.debug = other.getDebug();
                this.debugFrame = other.getDebugFrame();
                this.pose = other.getPose();
                this.detectedRefernce = other.getDetectedRefernce();
                this.boundingRec = other.getBoundingRec();
                this.objectContours = other.getObjectContours();
                this.contour = other.getContour();
                this.transformationMat = other.getTransformationMat();
            }
        }
    }

    public Point getPose() {
        Lock l = poseLock.readLock();
        l.lock();
        Point v = this.pose;
        l.unlock();
        return v;
    }

    public void setPose(Point pose) {
        Lock l = poseLock.writeLock();
        l.lock();
        this.pose = pose;
        l.unlock();
    }

    public RefernceModells getDetectedRefernce() {
        Lock l = detectedRefernceLock.readLock();
        l.lock();
        RefernceModells v = this.detectedRefernce;
        l.unlock();
        return v;
    }

    public void setDetectedRefernce(RefernceModells detectedRefernce) {
        Lock l = detectedRefernceLock.writeLock();
        l.lock();
        this.detectedRefernce = detectedRefernce;
        l.unlock();
    }

    public Mat getDebugFrame() {
        Lock l = debugFrameLock.readLock();
        l.lock();
        Mat v = null;
        v = this.debugFrame;
        l.unlock();
        return v;
    }

    public void setDebugFrame(Mat debugFrame) {
        Lock l = debugFrameLock.writeLock();
        l.lock();
        this.debugFrame = debugFrame;
        l.unlock();
    }

    public boolean getDebug() {
        Lock l = debugLock.readLock();
        l.lock();
        Boolean v = this.debug;
        l.unlock();
        return v;
    }

    public void setDebug(boolean debug) {
        Lock l = debugLock.writeLock();
        l.lock();
        this.debug = debug;
        l.unlock();
    }

    public Rect getBoundingRec() {
        Lock l = boundingRecLock.readLock();
        l.lock();
        Rect v = boundingRec;
        l.unlock();
        return v;
    }

    public void setBoundingRec(Rect boundingRec) {
        Lock l = boundingRecLock.writeLock();
        l.lock();
        this.boundingRec = boundingRec;
        l.unlock();
    }

    public List<MatOfPoint> getObjectContours() {
        Lock l = objectContoursLock.readLock();
        l.lock();
        List<MatOfPoint> v = objectContours;
        l.unlock();
        return v;
    }

    public void setObjectContours(List<MatOfPoint> objectContours) {
        Lock l = objectContoursLock.writeLock();
        l.lock();
        this.objectContours = objectContours;
        l.unlock();
    }

    public List<Point> getContour() {
        Lock l = contourLock.readLock();
        l.lock();
        List<Point> con = contour;
        l.unlock();
        return con;
    }

    public void setContour(List<Point> contour) {
        Lock l = contourLock.writeLock();
        l.lock();
        this.contour = contour;
        l.unlock();
    }

    public Mat getTransformationMat() {
        Lock l = transformationLock.readLock();
        l.lock();
        Mat m = transformationMat;
        l.unlock();
        return m;

    }

    public void setTransformationMat(Mat transformationMat) {
        Lock l = transformationLock.readLock();
        l.lock();
        this.transformationMat = transformationMat;
        l.unlock();
    }
}
