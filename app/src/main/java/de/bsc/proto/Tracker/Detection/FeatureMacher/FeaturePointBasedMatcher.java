package de.bsc.proto.Tracker.Detection.FeatureMacher;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.DMatch;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.features2d.BFMatcher;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.ORB;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.List;

import de.bsc.proto.Database.Modell.RefernceModells;
import de.bsc.proto.Tracker.Detection.BaseMatcher;
import de.bsc.proto.Tracker.Detection.ContourMatchingDescriptor;

import static java.lang.Math.max;
import static org.opencv.calib3d.Calib3d.RANSAC;
import static org.opencv.calib3d.Calib3d.findHomography;
import static org.opencv.core.Core.NORM_HAMMING;
import static org.opencv.core.Core.NORM_HAMMING2;
import static org.opencv.features2d.ORB.HARRIS_SCORE;

/**
 * Created by RobNoFlop on 17.01.2016.
 */
public class FeaturePointBasedMatcher extends BaseMatcher {


    public boolean performORBDetection(RefernceModells modell, Mat currentFrame, ContourMatchingDescriptor refContourMatchingDescriptor) {
        image = currentFrame;
        match = false;

        MatOfKeyPoint frameKexPoints = new MatOfKeyPoint();
        Mat frameDeskriptors = new Mat();

        ORB detector = ORB.create(4000, 1.2f, 8, 31, 0, 2, HARRIS_SCORE, 31, 20);
        //FeatureDetector detector = FeatureDetector.create(FeatureDetector.ORB);
        //detector.read("app/src/main/assets/detectorSettings.yml");

        DescriptorExtractor extractor = DescriptorExtractor.create(DescriptorExtractor.ORB);

        detector.detect(image, frameKexPoints);
        extractor.compute(image, frameKexPoints, frameDeskriptors);
        contourMatchingDescriptor = ContourMatchingDescriptor.buildFromKeyPoints(null, image.cols(), image.rows(), frameKexPoints);

        Mat objectDeskriptors = readORBDescriptor(modell);

        if (objectDeskriptors != null) {
            List<DMatch> good_matches = calulateGoodOrbMatches(frameDeskriptors, objectDeskriptors);
            Log.d(this.getClass().toString(), "performORBDetection: good_matches " + good_matches.size() );
            if (good_matches.size() > 1000) {
                match = true;
                findBoundingRect(good_matches, frameKexPoints);
                findTransformdContour(good_matches, frameKexPoints, refContourMatchingDescriptor);

            }
        }
        return match;
    }

    private Mat readORBDescriptor(RefernceModells modell) {
        Bitmap img = BitmapFactory.decodeByteArray(modell.getDescriptors(), 0, modell.getDescriptors().length);
        Bitmap bmp32 = img.copy(Bitmap.Config.ARGB_8888, true);

        Mat objectDeskriptors = new Mat();
        Utils.bitmapToMat(bmp32, objectDeskriptors);
        List<Mat> channels = new ArrayList<>();
        Core.split(objectDeskriptors, channels);

        Mat help = new Mat();
        Imgproc.cvtColor(objectDeskriptors, help, Imgproc.COLOR_BGRA2GRAY);
        objectDeskriptors = help;
        return objectDeskriptors;
    }

    private List<DMatch> calulateGoodOrbMatches(Mat frameDeskriptors, Mat objectDeskriptors) {
        BFMatcher matcher = BFMatcher.create(NORM_HAMMING2, false); //DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
        MatOfDMatch matches = new MatOfDMatch();
        MatOfDMatch matchesCross = new MatOfDMatch();

        matcher.match(objectDeskriptors, frameDeskriptors, matches);
        matcher.match(frameDeskriptors, objectDeskriptors, matchesCross);

        List<DMatch> good_matches = new ArrayList<DMatch>();

        List<DMatch> matchesList = matches.toList();
        List<DMatch> matchesCossList = matchesCross.toList();

        for (int i = 0; i < matchesList.size(); i++) {
            DMatch forward = matchesList.get(i);
            DMatch backward = matchesCossList.get(forward.trainIdx);
            if (backward.trainIdx == forward.queryIdx) {
                good_matches.add(forward);
            }
        }

        return good_matches;
    }

    private void findBoundingRect(List<DMatch> good_matches, MatOfKeyPoint frameKexPoints) {

        KeyPoint[] frameKexPointsArray = frameKexPoints.toArray();
        List<Point> machtedKeyPoints = new ArrayList<>();

        for (DMatch m : good_matches) {
            machtedKeyPoints.add(frameKexPointsArray[m.trainIdx].pt);
        }

        MatOfPoint m = new MatOfPoint();
        m.fromList(machtedKeyPoints);
        boundingRec = Imgproc.boundingRect(m);
        int x = boundingRec.x + boundingRec.width / 2;
        int y = boundingRec.y + boundingRec.height / 2;
        pos = new Point(x,y);
    }

    private void findTransformdContour(List<DMatch> good_matches,
                                       MatOfKeyPoint frameKexPoints,
                                       ContourMatchingDescriptor refContourMatchingDescriptor) {
        ArrayList<Point> matchedRefPoints = new ArrayList<>();
        ArrayList<Point> matchedPoints = new ArrayList<>();
        KeyPoint[] framePointsArray = frameKexPoints.toArray();
        for (DMatch match : good_matches) {
            //ToDO:
            if (match.trainIdx < framePointsArray.length) {
                matchedPoints.add(framePointsArray[match.trainIdx].pt);
                Point refPoint = refContourMatchingDescriptor.getMatchingPoints().get(match.queryIdx);
                matchedRefPoints.add(refPoint);
            }
        }

//        MatOfPoint2f matchedPointsMat = new MatOfPoint2f();
//        matchedPointsMat.fromList(matchedPoints);
//        MatOfPoint2f matchedRefPointsMat = new MatOfPoint2f();
//        matchedRefPointsMat.fromList(matchedRefPoints);
//        transformMatrix = findHomography(matchedPointsMat, matchedRefPointsMat, RANSAC, 3);
//        tranformMatrix = findHomography(matchedRefPointsMat, matchedRefPointsMat);
//        tranformMatrix = findHomography(matchedPointsMat, matchedPointsMat);


        MatOfPoint matchedPointsMat2 = new MatOfPoint();
        matchedPointsMat2.fromList(matchedPoints);
        MatOfPoint matchedRefPointsMat2 = new MatOfPoint();
        matchedRefPointsMat2.fromList(matchedRefPoints);
        Rect refRect = Imgproc.boundingRect(matchedRefPointsMat2);
        Rect sceneRect = Imgproc.boundingRect(matchedPointsMat2);
        ArrayList<Point> matchedRefPoints2 = new ArrayList<>();
        matchedRefPoints2.add(new Point(refRect.x, refRect.y));
        matchedRefPoints2.add(new Point(refRect.x + refRect.width, refRect.y));
        matchedRefPoints2.add(new Point(refRect.x, refRect.y + refRect.height));
        matchedRefPoints2.add(new Point(refRect.x + refRect.width, refRect.y + refRect.height));
        ArrayList<Point> matchedPoints2 = new ArrayList<>();
        matchedPoints2.add(new Point(sceneRect.x, sceneRect.y));
        matchedPoints2.add(new Point(sceneRect.x + sceneRect.width, sceneRect.y));
        matchedPoints2.add(new Point(sceneRect.x, sceneRect.y + sceneRect.height));
        matchedPoints2.add(new Point(sceneRect.x + sceneRect.width, sceneRect.y + sceneRect.height));

        MatOfPoint2f matchedPointsMat3 = new MatOfPoint2f();
        matchedPointsMat3.fromList(matchedPoints2);
        MatOfPoint2f matchedRefPointsMat3 = new MatOfPoint2f();
        matchedRefPointsMat3.fromList(matchedRefPoints2);

        transformMatrix = findHomography(matchedRefPointsMat3, matchedPointsMat3);

        List<Point> contour = refContourMatchingDescriptor.getContour().toList();
        ArrayList<Point> trasContour = new ArrayList<>();

        Mat transformed = new Mat();
        Core.perspectiveTransform(Converters.vector_Point2f_to_Mat(contour), transformed, transformMatrix);
        Converters.Mat_to_vector_Point2f(transformed, trasContour);

        this.contour = trasContour;
    }
}
