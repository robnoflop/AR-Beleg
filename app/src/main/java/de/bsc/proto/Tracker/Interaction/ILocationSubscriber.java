package de.bsc.proto.Tracker.Interaction;

import android.location.Location;

/**
 * Created by RobNoFlop on 26.11.2015.
 */
public interface ILocationSubscriber {
    void loactionUpdate(Location newLoaction);
}
