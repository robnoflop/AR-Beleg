package de.bsc.proto.Tracker.Interaction;

/**
 * Created by RobNoFlop on 05.01.2016.
 */
public interface IOrientationSubscriber {
    void orientationUpdate(double newOrientation);
}
