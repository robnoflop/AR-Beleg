package de.bsc.proto.Tracker.Interaction;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;

/**
 * Created by RobNoFlop on 26.11.2015.
 */
public class SensorManagement implements OnLocationUpdatedListener, SensorEventListener {
    // Acquire a reference to the system Location Manager
    private SensorManager sensorManager;

    private Sensor accelerometer;

    private Sensor magnetometer;

    private int current_orientation = 0; // orientation received by onOrientationChanged

    private double level_angle = 0.0f;

    private final float sensor_alpha = 0.8f; // for filter

    private boolean has_gravity = false;

    private float[] gravity = new float[3];

    private boolean has_geomagnetic = false;

    private float[] geomagnetic = new float[3];

    private float[] deviceRotation = new float[9];

    private float[] cameraRotation = new float[9];

    private float[] deviceInclination = new float[9];

    private float[] geo_direction = new float[3];

    private Location location;

    private double orientation = 0.0F;

    private double arcOrientation = 0.0F;

    private List<ILocationSubscriber> locationSubscribers = new ArrayList<>();

    private List<IOrientationSubscriber> orientationSubscribers = new ArrayList<>();

    private long lastOrientationSubscribersUpdate = 0;

    private int orientationSubscriberupdateIntervall = 1000;

    private final int locationUpddateDelta = 25;

    private final int orientationUpddateDelta = 5;

    private void setLocation(Location location) {
        this.location = location;
    }

    private void setOrientation(float orientation) {
        this.orientation = orientation;
    }

    public SensorManagement(SensorManager sensorManager) {
        try {
            this.sensorManager = sensorManager;

            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void start(Context context) {
        float trackingDistance = 5;
        LocationAccuracy trackingAccuracy = LocationAccuracy.HIGH;

        LocationParams.Builder builder = new LocationParams.Builder()
                .setAccuracy(trackingAccuracy)
                .setDistance(trackingDistance);
        SmartLocation.with(context).location().continuous().config(builder.build()).start(this);

        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    public void stop(Context context) {
        SmartLocation.with(context).location().stop();

        sensorManager.unregisterListener(this);
    }

    public void subscribe(ILocationSubscriber sub) {
        locationSubscribers.add(sub);
    }

    public void subscribe(IOrientationSubscriber sub) {
        orientationSubscribers.add(sub);
    }

    public void unsubscribe(ILocationSubscriber sub) {
        locationSubscribers.remove(sub);
    }

    public void unsubscribe(IOrientationSubscriber sub) {
        orientationSubscribers.remove(sub);
    }

    @Override
    public void onLocationUpdated(Location location) {
        setLocation(location);
        updateLocatioSubscribers();
    }

    private void updateLocatioSubscribers() {
        for (ILocationSubscriber sub : locationSubscribers) {
            sub.loactionUpdate(this.location);
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            onAccelerometerSensorChanged(event);
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            onMagneticSensorChanged(event);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /* Implementation from OpenCamera*/
    public void onAccelerometerSensorChanged(SensorEvent event) {
        this.has_gravity = true;
        for (int i = 0; i < 3; i++) {
            this.gravity[i] = sensor_alpha * this.gravity[i] + (1.0f - sensor_alpha) * event.values[i];
        }
        calculateGeoDirection();

        double x = gravity[0];
        double y = gravity[1];
        this.level_angle = Math.atan2(-x, y) * 180.0 / Math.PI;
        if (this.level_angle < -0.0) {
            this.level_angle += 360.0;
        }
        this.level_angle -= (float) this.current_orientation;
        if (this.level_angle < -180.0) {
            this.level_angle += 360.0;
        } else if (this.level_angle > 180.0) {
            this.level_angle -= 360.0;
        }
    }

    /* Implementation from OpenCamera*/
    public void onMagneticSensorChanged(SensorEvent event) {
        this.has_geomagnetic = true;
        for (int i = 0; i < 3; i++) {
            //this.geomagnetic[i] = event.values[i];
            this.geomagnetic[i] = sensor_alpha * this.geomagnetic[i] + (1.0f - sensor_alpha) * event.values[i];
        }
        calculateGeoDirection();
    }

    /* Implementation from OpenCamera*/
    private void calculateGeoDirection() {
        if (!this.has_gravity || !this.has_geomagnetic) {
            return;
        }
        if (!SensorManager.getRotationMatrix(this.deviceRotation, this.deviceInclination, this.gravity, this.geomagnetic)) {
            return;
        }
        SensorManager.remapCoordinateSystem(this.deviceRotation, SensorManager.AXIS_X, SensorManager.AXIS_Z, this.cameraRotation);
        SensorManager.getOrientation(cameraRotation, geo_direction);

        double geo_angle = Math.toDegrees(geo_direction[0]);
        if (geo_angle < 0.0f) {
            geo_angle += 360.0f;
        }

        if (Math.toDegrees(Math.abs(geo_direction[0] - arcOrientation)) > orientationUpddateDelta) {
            arcOrientation = geo_direction[0];
            orientation = geo_angle;
            updateOrientationSubcribers();
        }
    }

    private synchronized void updateOrientationSubcribers() {
        if (System.currentTimeMillis() - lastOrientationSubscribersUpdate > orientationSubscriberupdateIntervall) {
            for (IOrientationSubscriber sub : orientationSubscribers) {
                sub.orientationUpdate(this.orientation);
            }
            lastOrientationSubscribersUpdate = System.currentTimeMillis();
        }
    }
}
