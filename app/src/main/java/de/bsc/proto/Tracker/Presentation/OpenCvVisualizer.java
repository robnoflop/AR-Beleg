package de.bsc.proto.Tracker.Presentation;


import android.support.v4.util.Pair;
import android.util.Log;

import com.orhanobut.logger.Logger;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.bsc.proto.Database.Modell.Informations;
import de.bsc.proto.Database.Modell.InformationsVisualisations;
import de.bsc.proto.Database.Modell.RefernceModells;
import de.bsc.proto.Database.Modell.RefernceModellsInformationsInformationsVisualisations;
import de.bsc.proto.Database.dao.DaoManager;
import de.bsc.proto.Tracker.Detection.DetectorResult;

/**
 * Created by RobNoFlop on 17.11.2015.
 */
public class OpenCvVisualizer extends Visualizer {

    private Mat currentFrame;

    private Boolean error = false;

    private List<RefernceModellsInformationsInformationsVisualisations> mivs;

    private Map<Integer, Informations> informationsMap;

    private Map<Integer, InformationsVisualisations> informationsVisualisationsMap;

    private Map<Integer, Toggle> informationsTooglesLoctionMap;

    private RefernceModells currentModell = new RefernceModells();

    public OpenCvVisualizer() {
    }

    public Mat renderToOutput(Mat frame, DetectorResult detectorResult) {
        currentFrame = frame;
        if (detectorResult != null) {
            try {
                updateVisulaisations(detectorResult);
                render(detectorResult);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return currentFrame;
    }

    private void updateVisulaisations(DetectorResult detectorResult) {
        if (currentModell != detectorResult.getDetectedRefernce()) {
            currentModell = detectorResult.getDetectedRefernce();
            informationsMap = new HashMap<>();
            informationsVisualisationsMap = new HashMap<>();
            informationsTooglesLoctionMap = new HashMap<>();

            mivs = DaoManager.mivDao.getMIVByRefernceModells(currentModell);
            for (RefernceModellsInformationsInformationsVisualisations miv : mivs) {
                Informations info = DaoManager.informationsDao.getById(miv.getInformationsRef());
                informationsMap.put(miv.getInformationsRef(), info);
                InformationsVisualisations infoVis = DaoManager.informationsVisualisationsDao.getById(miv.getInformationsVisualisationsRef());
                informationsVisualisationsMap.put(miv.getInformationsVisualisationsRef(), infoVis);
                Point position = new Point(miv.getPositionX(), miv.getPositionY());
                informationsTooglesLoctionMap.put(miv.getInformationsRef(), new Toggle(position));
            }
        }
        if (detectorResult == null) {
            currentModell = null;
            mivs.clear();
            informationsMap.clear();
            informationsVisualisationsMap.clear();
            informationsTooglesLoctionMap.clear();
        }
    }

    private void render(DetectorResult detectorResult) {
        if (detectorResult.getDebug()) {
            renderDebugResult(detectorResult);
        } else {
            renderResult(detectorResult);
        }
    }

    private void renderDebugResult(DetectorResult detectorResult) {
        currentFrame = detectorResult.getDebugFrame();
        if (currentFrame.channels() == 1) {
            Imgproc.cvtColor(currentFrame, currentFrame, Imgproc.COLOR_GRAY2BGRA);
        }
    }

    @Override
    protected void renderResult(DetectorResult detectorResult) {
        super.renderResult(detectorResult);
        if (currenDetectorResult.getContour() != null) {
            List<Point> contour = currenDetectorResult.getContour();
            Mat transformed = new Mat();
            ArrayList<Point> trasContour = new ArrayList<>();
            Core.perspectiveTransform(Converters.vector_Point2f_to_Mat(contour), transformed, currenDetectorResult.getTransformationMat());
            Converters.Mat_to_vector_Point2f(transformed, trasContour);

            renderContour(trasContour);
        }
        else {
            renderBoundingBox(currenDetectorResult.getBoundingRec());
        }

        if (mivs.size() == 1) {
            renderInformation(mivs.get(0), currenDetectorResult.getPose());
        } else if (mivs.size() > 1) {
            renderInfromationBar();
        }
    }

    private void renderBoundingBox(Rect boundingRec) {
        if (boundingRec != null) {
            Imgproc.rectangle(currentFrame, boundingRec.tl(), boundingRec.br(), black, 6);
            Imgproc.rectangle(currentFrame, boundingRec.tl(), boundingRec.br(), dimGray, 2);
        }
    }

    private void renderContour(List<Point> contour) {
        for (int i = 0; i < contour.size() - 1; i++) {
            Imgproc.line(currentFrame, contour.get(i), contour.get(i + 1), black, 6);
        }
    }

    private void renderInfromationBar() {
        Point pose = new Point(60, 60);
        for (RefernceModellsInformationsInformationsVisualisations miv : mivs) {
            renderInformation(miv, pose);
            pose.y += 30;
        }
    }

    private void renderInformation(RefernceModellsInformationsInformationsVisualisations miv, Point pose) {
        if (pose == null) {
            return;
        }
        if (informationsVisualisationsMap.get(miv.getInformationsVisualisationsRef()).getMethod().equals("Text"))
        {
            Imgproc.putText(currentFrame, informationsMap.get(miv.getInformationsRef()).getText(), pose, 0, 0.75, black, 6);
            Imgproc.putText(currentFrame, informationsMap.get(miv.getInformationsRef()).getText(), pose, 0, 0.75, withe, 2);
        }
        else if (informationsVisualisationsMap.get(miv.getInformationsVisualisationsRef()).getMethod().equals("Toggle"))
        {
            Point p = informationsTooglesLoctionMap.get(miv.getInformationsRef()).point.clone();
            Mat transformed = new Mat();
            ArrayList<Point> pList = new ArrayList<>();
            if (p.x < currenDetectorResult.getBoundingRec().width) {
                p.x += currenDetectorResult.getBoundingRec().x;
            } else {
                p.x /= currenDetectorResult.getBoundingRec().width;
                p.x += currenDetectorResult.getBoundingRec().x;
            }

            if (p.y < currenDetectorResult.getBoundingRec().height) {
                p.y += currenDetectorResult.getBoundingRec().y;
            } else {
                p.y /= currenDetectorResult.getBoundingRec().height;
                p.y += currenDetectorResult.getBoundingRec().y;
            }
            pList.add(p);
            Core.perspectiveTransform(Converters.vector_Point2f_to_Mat(pList), transformed, currenDetectorResult.getTransformationMat());
            ArrayList<Point> trasPoint = new ArrayList<>();
            Converters.Mat_to_vector_Point2f(transformed, trasPoint);
            if (informationsTooglesLoctionMap.get(miv.getInformationsRef()).active) {
                renderActiveActionPoint(trasPoint.get(0));
                Point textPoint = new Point(trasPoint.get(0).x + 50, trasPoint.get(0).y);
                Imgproc.putText(currentFrame, informationsMap.get(miv.getInformationsRef()).getText(), textPoint, 0, 0.75, black, 6);
                Imgproc.putText(currentFrame, informationsMap.get(miv.getInformationsRef()).getText(), textPoint, 0, 0.75, withe, 2);
            } else {
                renderInactiveActionPoint(trasPoint.get(0));
            }
        }
    }

    @Override
    public void renderInactiveActionPoint(Point point) {
        Imgproc.circle(currentFrame, point, 15, actionPointColor);
        Imgproc.circle(currentFrame, point, 20, actionPointColor, 5);
    }

    @Override
    public void renderActiveActionPoint(Point point) {
        Imgproc.circle(currentFrame, point, 10, actionPointColor, -1);
        Imgproc.circle(currentFrame, point, 15, actionPointColor);
    }

    public void updateActionLocation(int x, int y) {
        super.updateActionLocation(x, y);
        if (informationsTooglesLoctionMap != null && currenDetectorResult != null) {
            for (Toggle t : informationsTooglesLoctionMap.values()) {
                if (isTouchNearActionPoint(t.point)) {
                    t.active = !t.active;
                }
            }
        }
    }

}

