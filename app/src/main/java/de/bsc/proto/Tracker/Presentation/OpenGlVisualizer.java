package de.bsc.proto.Tracker.Presentation;

import android.opengl.GLSurfaceView;

import org.opencv.core.Mat;
import org.opencv.core.Point;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import de.bsc.proto.Tracker.Detection.DetectorResult;

/**
 * Created by RobNoFlop on 19.01.2016.
 */
public class OpenGlVisualizer extends Visualizer implements GLSurfaceView.Renderer{

    @Override
    public Mat renderToOutput(Mat frame, DetectorResult detectorResult) {
        return null;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

    }

    @Override
    public void onDrawFrame(GL10 gl) {

    }

    @Override
    public void renderInactiveActionPoint(Point point) {

    }

    @Override
    public void renderActiveActionPoint(Point point) {

    }
}
