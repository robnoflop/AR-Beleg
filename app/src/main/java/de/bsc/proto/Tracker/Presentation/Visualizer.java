package de.bsc.proto.Tracker.Presentation;

import com.orhanobut.logger.Logger;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

import de.bsc.proto.Tracker.Detection.DetectorResult;

/**
 * Created by RobNoFlop on 19.01.2016.
 */
public abstract class Visualizer {

    protected static final int ACTION_DISTANCE_THRESHOLD = 100;

    protected int xAction = -1;
    protected int yAction = -1;

    protected DetectorResult currenDetectorResult;

    protected Scalar withe = new Scalar(255, 255, 255);
    protected Scalar gray = new Scalar(190, 190, 190);
    protected Scalar dimGray = new Scalar(105, 105, 105);
    protected Scalar black = new Scalar(0, 0, 0);

    protected Scalar actionPointColor = new Scalar(1,121,111);

    public abstract Mat renderToOutput(Mat frame, DetectorResult detectorResult);
    public abstract void renderInactiveActionPoint(Point point);
    public abstract void renderActiveActionPoint(Point point);

    public void updateActionLocation(int x, int y) {


        xAction = x;
        yAction = y;
        Logger.d("Location: x: " + x + ", y: " + y);
    }

    protected void renderResult(DetectorResult detectorResult) {
        currenDetectorResult = detectorResult;
    }

    protected boolean isTouchNearActionPoint(Point point) {
        boolean isNear = false;
        double distance = Math.abs(xAction - point.x) + Math.abs(yAction - point.y);
        if (distance <= ACTION_DISTANCE_THRESHOLD) {
            isNear = true;
        }
        return isNear;
    }
}
