package de.bsc.proto.Tracker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.List;

import de.bsc.proto.Database.Modell.Tours;
import de.bsc.proto.R;
import de.bsc.proto.Tracker.Detection.Detector;
import de.bsc.proto.Tracker.Detection.DetectorResult;
import de.bsc.proto.Tracker.Presentation.OpenCvVisualizer;
import de.bsc.proto.Tracker.Presentation.Visualizer;
import de.bsc.proto.Views.ExtentedJavaCameraView;


/**
 * Created by RobNoFlop on 17.11.2015.
 */
public class Tracker
        extends Thread
        implements CameraBridgeViewBase.CvCameraViewListener2,
                View.OnTouchListener,
                View.OnDragListener
{

    private Detector detector;

    private Context context;

    private Visualizer vis;

    private Tours tour;

    private ExtentedJavaCameraView cameraView;

    private DisplayMetrics screenSize;

    public Tracker(Tours tour, ExtentedJavaCameraView cameraView, Context context) {
        this.tour = tour;
        this.context = context;
        this.vis = new OpenCvVisualizer();

        //this.vis = new OpenGlVisualizer();

        this.detector = new Detector(context, tour);
        this.cameraView = cameraView;
        screenSize = context.getResources().getDisplayMetrics();
    }

    public void onCameraViewStarted(int width, int height) {
        List<Camera.Size> resolutions = cameraView.getResolutionList();
        double help = (double)screenSize.heightPixels / (double)screenSize.widthPixels;
        double format = Math.round(help * 10.0) / 10.0;
        Camera.Size res = null;
        for (Camera.Size s : resolutions) {
            if (format == Math.round((double)s.height / (double)s.width * 10.0) / 10.0) {
                res = s;
                break;
            }
        }
        if (res != null) {
            cameraView.setResolution(res);
        }
        detector.start();
    }

    public void onCameraViewStopped() {
        detector.stop();
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat currentFrame = inputFrame.rgba();

        detector.detect(currentFrame);
        DetectorResult r = detector.getResult();
        if (r != null) {
             int debug = 0;
            debug++;
        }
        return vis.renderToOutput(currentFrame, r);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        updateVisuliserActionLoaction(event.getX(), event.getY());
        return false;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        updateVisuliserActionLoaction(event.getX(), event.getY());
        return false;
    }

    private void updateVisuliserActionLoaction(float x, float y) {
        Size cameraSize = cameraView.getResolution();
        int xCamera = (int) (cameraSize.width / screenSize.widthPixels  * x);
        int yCamera = (int) (cameraSize.height / screenSize.heightPixels * y);
        xCamera -= Math.abs((screenSize.widthPixels - cameraSize.width));
        yCamera -= Math.abs((screenSize.heightPixels - cameraSize.height));
        vis.updateActionLocation(xCamera, yCamera);
    }
}
