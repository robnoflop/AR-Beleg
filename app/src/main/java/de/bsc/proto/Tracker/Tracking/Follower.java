package de.bsc.proto.Tracker.Tracking;

import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Point;

import java.util.ArrayList;

/**
 * Created by RobNoFlop on 21.12.2015.
 */
public abstract class Follower {

    protected boolean isFollowring = false;

    protected Rect boundRect;

    protected Mat transformation;

    protected Point pose;

    public boolean isFollowring() {
        return isFollowring;
    }

    public Rect getBoundRect() {
        return boundRect;
    }

    public Point getPose() {
        return pose;
    }

    public Mat getTransformation() {
        return transformation;
    }

    public abstract void follow(Mat newFrame);



}
