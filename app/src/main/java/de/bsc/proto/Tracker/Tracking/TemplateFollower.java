package de.bsc.proto.Tracker.Tracking;

import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Size;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import static org.opencv.calib3d.Calib3d.RANSAC;
import static org.opencv.calib3d.Calib3d.findHomography;

/**
 * Created by RobNoFlop on 17.01.2016.
 */
public class TemplateFollower extends Follower {

    private Mat template;

    private static final int shrinkFaktor = 6;

    private Mat result;

    private MinMaxLocResult minMaxLocResult;

    public TemplateFollower(Mat frame, Rect boundingRect) {
        isFollowring = true;
        boundRect = boundingRect;
        template = extractTemplate(frame, boundingRect);
        template = shinkMat(template);
        Size resultSize = new Size(frame.width() - template.width() + 1,
                frame.height() - template.height() + 1);

        result = new Mat(resultSize, CvType.CV_32FC1);
    }

    String tag = "follow";

    @Override
    public void follow(Mat newFrame) {
        newFrame = shinkMat(newFrame);
        Imgproc.GaussianBlur(newFrame, newFrame, new Size(7, 7), 3.0);

        Imgproc.matchTemplate(newFrame, template, result, Imgproc.TM_CCOEFF_NORMED);

        Imgproc.threshold(result, result, 0.1, 1., Imgproc.THRESH_TOZERO);
        minMaxLocResult = Core.minMaxLoc(result);
        if (extraxtPose(minMaxLocResult)) {
            //template = extractTemplate(newFrame, boundRect);
            growPose();
        } else {
            isFollowring = false;
            result.release();
            template.release();
        }
        newFrame.release();
    }

    private void growPose() {
        boundRect.height *= shrinkFaktor;
        boundRect.width *= shrinkFaktor;
        boundRect.x *= shrinkFaktor;
        boundRect.y *= shrinkFaktor;
        pose.x *= shrinkFaktor;
        pose.y *= shrinkFaktor;
    }

    private Mat shinkMat(Mat mat){
        Mat resizeimage = new Mat();
        Size sz = new Size(mat.width() / shrinkFaktor, mat.height() / shrinkFaktor);
        Imgproc.resize( mat, resizeimage, sz );
        return resizeimage;
    }

    private boolean extraxtPose(MinMaxLocResult minMaxLocResult) {
        boolean succress = false;
        if (minMaxLocResult.maxVal > 0.0 &&
                minMaxLocResult.maxVal != minMaxLocResult.minVal &&
                minMaxLocResult.maxVal > 0.5) {
            pose = minMaxLocResult.maxLoc;
            double x1 = pose.x;// + template.width() / 2;
            double y1 = pose.y;// + template.height() / 2;
            double x2 = pose.x + template.width();// / 2;
            double y2 = pose.y + template.height();// / 2;

            Mat tranformMatrix = findHomography(
                    new MatOfPoint2f(
                        new Point(boundRect.x, boundRect.y),
                        new Point(boundRect.x + boundRect.width, boundRect.y),
                        new Point(boundRect.x, boundRect.y + boundRect.height),
                        new Point(boundRect.x + boundRect.width, boundRect.y + boundRect.height)),
                    new MatOfPoint2f(
                        new Point(x1 * shrinkFaktor, y1 * shrinkFaktor),
                        new Point(x2 * shrinkFaktor, y1 * shrinkFaktor),
                        new Point(x1 * shrinkFaktor, y2 * shrinkFaktor),
                        new Point(x2 * shrinkFaktor, y2 * shrinkFaktor)));

            transformation = tranformMatrix;
            boundRect = new Rect(new Point(x1, y1), new Point(x2, y2));

            succress = true;
        }
        return succress;
    }

    private Mat extractTemplate(Mat frame, Rect roi) {
        roi = cropRoiToFrameBorders(frame, roi);
        frame = frame.submat(roi);
        Core.normalize(frame, frame, 0, 1, Core.NORM_MINMAX, -1);
        return frame;
    }

    private Rect cropRoiToFrameBorders(Mat frame, Rect roi) {
        if (roi.x < 0) {
            roi.width += roi.x;
            roi.x = 0;
        }

        if (roi.y < 0) {
            roi.height += roi.y;
            roi.y = 0;
        }

        if (roi.x + roi.width > frame.width()) {
            roi.width -= roi.x + roi.width - frame.width();
        }

        if (roi.y + roi.height > frame.height()) {
            roi.height -= roi.y + roi.height - frame.height();
        }

        return roi;
    }
}