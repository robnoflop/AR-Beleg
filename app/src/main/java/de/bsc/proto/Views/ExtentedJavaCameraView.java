package de.bsc.proto.Views;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;

import org.opencv.android.JavaCameraView;
import org.opencv.core.Size;

import java.util.List;

//http://answers.opencv.org/question/7313/rotating-android-camera-to-portrait/
//http://answers.opencv.org/users/7519/zarokka/

public class ExtentedJavaCameraView extends JavaCameraView {

    private String mPictureFileName;

    public ExtentedJavaCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public List<Camera.Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPreviewSizes();
    }

    public void setResolution(Camera.Size resolution) {
        disconnectCamera();
        mMaxHeight = resolution.height;
        mMaxWidth = resolution.width;
        connectCamera(getWidth(), getHeight());
    }

    public Size getResolution() {
        return new Size(mMaxWidth, mMaxHeight);
    }
}