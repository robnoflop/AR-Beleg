package de.bsc.proto.utils;

import android.location.Location;

import de.bsc.proto.Database.Modell.Objects;

/**
 * Created by RobNoFlop on 18/10/2017.
 */

public class TourObjectLocationUtils {

    public static final int OBJECTS_IN_AREA_DISTANCE = 300;
    public static final int OBJECTS_IN_AREA_VISIBLE_DISTANCE = 150;

    public static boolean isInTourObjectRange(Location location, Objects tourObject) {
        boolean inRange = false;
        double distance = location.distanceTo(tourObject.getLocation());
        if (distance <= OBJECTS_IN_AREA_DISTANCE) {
            inRange = true;
        }
        return inRange;
    }

    public static boolean isInTourObjectRangeVisibleRange(Location location, Objects tourObject) {
        boolean inRange = false;
        double distance = location.distanceTo(tourObject.getLocation());
        if (distance <= OBJECTS_IN_AREA_VISIBLE_DISTANCE) {
            inRange = true;
        }
        return inRange;
    }

    public static  int viewDistance(int a, int b) {
        int d = Math.abs(a - b) % 360;
        return d > 180 ? 360 - d : d;
    }
}
